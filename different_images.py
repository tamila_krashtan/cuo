import cuo
import time
import os
import sys
from PIL import Image

N1 = 18
N2 = 18
path = "images/"

created_path = path+str(N1)+"-"+str(N2)+"m+c/"
if not os.path.exists(created_path):
    os.makedirs(created_path)

for i in range(N1, N2+1):
    start = time.time()
    cuo.colorize(path+str(i)+"/original.bmp",
             path+str(i)+"/marked.bmp",
             path+str(i)+"/colorized.bmp",
             "bmp", m=0)
    finish = time.time()

    print i, "colorized in time", finish-start

    start = time.time()
    cuo.colorize(path + str(i) + "/original.bmp",
                 path + str(i) + "/marked.bmp",
                 path + str(i) + "/colorized_wo_markers.bmp",
                 "bmp")
    finish = time.time()

    print i, "colorized without markers in time", finish-start

    # gather pairs marked+colorized+c_wo_m

    images = map(Image.open, [path+str(i)+"/marked.bmp", path+str(i)+"/colorized.bmp", path+str(i)+"/colorized_wo_markers.bmp"])
    widths, heights = zip(*(j.size for j in images))

    total_width = sum(widths)
    max_height = max(heights)

    new_im = Image.new('RGB', (total_width, max_height))

    x_offset = 0
    for im in images:
        new_im.paste(im, (x_offset, 0))
        x_offset += im.size[0]

    new_im.save(created_path + str(i) + ".bmp")

    print i, "done"
