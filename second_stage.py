import two_stage
import time
import os
import sys
from PIL import Image

N1 = 19
N2 = 19
path = "images/"

created_path = path+str(N1)+"-"+str(N2)+"_two_stage/"
if not os.path.exists(created_path):
    os.makedirs(created_path)

for i in range(N1, N2+1):
    two_stage.colorize(path + str(i) + "/original.bmp",
                 path + str(i) + "/marked.bmp",
                 path + str(i) + "/colorized.bmp",
                 "bmp")

    print i, "colorized"

    # gather pairs marked+colorized+c_wo_m

    images = map(Image.open, [path + str(i) + "/marked.bmp", path + str(i) + "/colorized_first.bmp",
                              path + str(i) + "/colorized_second.bmp"])
    widths, heights = zip(*(j.size for j in images))

    total_width = sum(widths)
    max_height = max(heights)

    new_im = Image.new('RGB', (total_width, max_height))

    x_offset = 0
    for im in images:
        new_im.paste(im, (x_offset, 0))
        x_offset += im.size[0]

    new_im.save(created_path + str(i) + ".bmp")

    print i, "done"