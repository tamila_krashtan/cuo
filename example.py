from scipy import misc
import matplotlib.pyplot as plt


def show_example(image):
    img = misc.imread(image)
    plt.imshow(img)
    plt.show()


if __name__ == "__main__":
    for img in ("original.bmp", "marked.bmp", "colorized.bmp"):
        show_example(img)