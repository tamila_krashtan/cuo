"""
This program is created to colorize black-and-white images.
To use it you need to prepare marked image - the same image as grayscale btu with strokes of predefined colors
Required packages: SciPy, MatPlotLib, NumPy and PIL
"""

from scipy import misc
from scipy import sparse
from scipy.sparse import linalg
import matplotlib.pyplot as plt
import numpy as np
import colorsys
from PIL import Image


def yiq_to_rgb(y, i, q):    # colorsys has troubles with arrays
    """
    The function converts image from YIQ to RGB
    :param y: luma information
    :param i: orange-blue range
    :param q: purple-green range
    :return: set of arrays representing image in RGB model
    """
    r = y + 0.956 * i + 0.623 * q
    g = y - 0.272 * i - 0.648 * q
    b = y - 1.105 * i + 1.705 * q

    for ch in (r, g, b):
        ch[ch > 1] = 1
        ch[ch < 0] = 0

    return r, g, b


def check_image(img):
    """
    Checking original image to be grayscale and uncorrupted. Otherwise raises appropriate exception
    :param img: path and name of examined image
    :return: returns nothing
    """
    try:
        imag = Image.open(img)
    except IOError:
        raise Exception("Corrupted image")

    imag = misc.imread(img)
    rg = abs(imag[:,:,0] - imag[:,:,1])
    rb = abs(imag[:,:,0] - imag[:,:,2])
    gb = abs(imag[:,:,1] - imag[:,:,2])

    diff = rg+rb+gb
    if max(diff.flatten())/255.0 > 0.1:
        raise Exception("Already colored image")


def info_from_img(o_img, m_img):
    """
    Obtains information about image by the names of original and marked variants
    :param o_img: path and name of original image
    :param m_img: path and name of marked image
    :return: combined YUV model representation as a float array and bool array showing pixels with defined colors
    """
    original = misc.imread(o_img)  # read image as numpy array
    marked = misc.imread(m_img)

    print original.shape, marked.shape

    #check_image(o_img)

    original = original.astype(float) / 255
    marked = marked.astype(float) / 255

    isColored = abs(original - marked).sum(2) > 0.01  # if pixel is colored - numpy array

    y, _, _ = colorsys.rgb_to_yiq(original[:, :, 0], original[:, :, 1], original[:, :, 2])
    _, i, q = colorsys.rgb_to_yiq(marked[:, :, 0], marked[:, :, 1], marked[:, :, 2])

    yuv = np.zeros(original.shape)
    yuv[:, :, 0] = y
    yuv[:, :, 1] = i
    yuv[:, :, 2] = q

    return yuv, isColored


def statistics(w_vals, w_index):
    """
    Making statistics calculations needed on each iteration
    :param w_vals: values in current window
    :param w_index: index of last added value
    :return: returns nothing, modifies vals array
    """
    variance = np.mean((w_vals[0:w_index + 1] - np.mean(w_vals[0:w_index + 1])) ** 2)
    sigma = variance * 0.6

    mgv = min((w_vals[0:w_index + 1] - w_vals[w_index]) ** 2)  # +1? - diff with closest
    if sigma < (-mgv / np.log(0.01)):
        sigma = mgv / np.log(0.01)
    if sigma < 0.000002:  # not to divide by 0
        sigma = 0.000002

    # weighting func
    w_vals[0:w_index] = np.exp(-((w_vals[0:w_index] - w_vals[w_index]) ** 2) / sigma)
    # sum of weighting funcs should be 1
    w_vals[0:w_index] = w_vals[0:w_index] / np.sum(w_vals[0:w_index])
    vals[length - w_index:length] = -w_vals[0:w_index]


def iterate():
    """
    One iteration of method
    :return: returns nothing, modifies vals, row_inds, col_inds
    """
    global length, pixel_num

    for j in range(width):
        for i in range(height):
            if not isColored[i, j]:
                window_index = 0
                window_vals = np.zeros(px_num_in_w)

                for ii in range(max(0, i-w_radius), min(i+w_radius+1, height)):
                    for jj in range(max(0, j - w_radius), min(j + w_radius + 1, width)):
                        if ii != i or jj != j:
                            row_inds[length] = pixel_num
                            col_inds[length] = indices[ii, jj]
                            window_vals[window_index] = YUV[ii, jj, 0]  # copy luma at ii, jj
                            length += 1
                            window_index += 1

                center = YUV[i, j, 0]
                window_vals[window_index] = center

                statistics(window_vals, window_index)

            row_inds[length] = pixel_num
            col_inds[length] = indices[i, j]
            vals[length] = 1

            length += 1
            pixel_num += 1


def solve():
    """
    Building sparse matrix, solving obtained linear equation system and building final YUV representation
    :return: colorized image in YUV model
    """
    global vals, col_inds, row_inds

    vals = vals[0:length]
    col_inds = col_inds[0:length]
    row_inds = row_inds[0:length]

    A = sparse.csr_matrix((vals, (row_inds, col_inds)), (pixel_num, size))  # matrix from sparse
    b = np.zeros((A.shape[0]))

    colorized = np.zeros((YUV.shape))
    colorized[:, :, 0] = YUV[:, :, 0]

    color_copy = isColored.reshape(size, order="F").copy()
    colored_indices = np.nonzero(color_copy)

    for ch in [1, 2]:
        channel_color = YUV[:, :, ch].reshape(size, order="F").copy()
        b[colored_indices] = channel_color[colored_indices]

        col_vals = linalg.spsolve(A, b)
        colorized[:, :, ch] = col_vals.reshape(height, width, order="F")

    return colorized


def has_more_than_4_neighbours(window, i, j):
    """
    more than 4 uncolored neighbours
    :param i:
    :param j:
    :return:
    """
    number = 0
    sum = [0, 0]
    for pair in [[0,7], [1,6], [2,5], [3,4]]:
        #print len(window),pair[0], pair[1], window
        if not window[pair[0]][0] and not window[pair[1]][0]:
            number += 1
            sum[0] += (colorized[window[pair[0]][1], window[pair[0]][2], 1] + colorized[window[pair[1]][1], window[pair[1]][2], 1])/2
            sum[1] += (colorized[window[pair[0]][1], window[pair[0]][2], 2] + colorized[window[pair[1]][1], window[pair[1]][2], 2])/2

    colorized[i, j, 1] = sum[0]/number
    colorized[i, j, 2] = sum[1]/number


def has_3_or_4_neighbours(window, i, j):
    """
    3 or 4 uncolored neighbours
    :param i:
    :param j:
    :return:
    """
    indices = [[0, 2, 7, 5],
               [[1,3], [1,4], [4,6], [3,6]],
               [[2,5], [0,7], [2,5], [7,0]]]

    corner = 0
    for num in range(4):
        if not window[indices[0][num]][0]:
            corner = num
            break

    if window[indices[1][corner][0]][0] or window[indices[1][corner][1]][0]:
        corner += 1
        if corner == 4:
            return
        if window[indices[1][corner][0]][0] or window[indices[1][corner][1]][0]:
            return

    corner0 = [colorized[window[indices[0][corner]][1], window[indices[0][corner]][2], 1],
               colorized[window[indices[0][corner]][1], window[indices[0][corner]][2], 2]]

    corner10 = [colorized[window[indices[1][corner][0]][1], window[indices[1][corner][0]][2], 1],
               colorized[window[indices[1][corner][0]][1], window[indices[1][corner][0]][2], 2]]

    corner11 = [colorized[window[indices[1][corner][1]][1], window[indices[1][corner][1]][2], 1],
                colorized[window[indices[1][corner][1]][1], window[indices[1][corner][1]][2], 2]]

    corner1 = [(corner10[0] + corner11[0]) / 2, (corner10[1] + corner11[1]) / 2]

    preresult = [2*corner1[0] - corner0[0], 2*corner1[1] - corner0[1]]
    corner2 = 0

    if not window[indices[2][corner][0]]:
        corner2 = [colorized[window[indices[2][corner][0]][1], window[indices[2][corner][0]][2], 1],
               colorized[window[indices[2][corner][0]][1], window[indices[2][corner][0]][2], 2]]
    else:
        if not window[indices[2][corner][1]]:
            corner2 = [colorized[window[indices[2][corner][1]][1], window[indices[2][corner][1]][2], 1],
                       colorized[window[indices[2][corner][1]][1], window[indices[2][corner][1]][2], 2]]

    if corner2:
        result = [(2*preresult[0]+corner2[0])/3, (2*preresult[1]+corner2[1])/3]
    else:
        result = preresult

    colorized[i, j, 1] = result[0]
    colorized[i, j, 2] = result[1]


def has_2_neighbours(window, i, j):
    """

    :param window:
    :param i:
    :param j:
    :return:
    """
    #print window
    number = 0
    sum = [0, 0]
    for pixel in window:
        if not pixel[0]:
            number += 1
            sum[0] += colorized[window[pixel[0]][1], window[pixel[0]][2], 1]
            sum[1] += colorized[window[pixel[0]][1], window[pixel[0]][2], 2]

    colorized[i, j, 1] = sum[0] / number
    colorized[i, j, 2] = sum[1] / number


def remove_markers():
    sparse = []
    for i in range(height):
        for j in range(width):
            if isColored[i, j]:
                sparse.append((i, j))

    drenno = False

    while sparse:
        l1 = len(sparse)
        print l1
        for pixel in sparse:
            i, j = pixel
            curr_window = []

            for ii in range(i-1, i+2):
                for jj in range(j-1, j+2):
                    if not ( ii==i  and jj==j):
                        if [ii, jj] in sparse or ii<0 or jj<0 or ii==height or jj==width:
                            curr_window.append([True, ii, jj])
                        else:
                            curr_window.append([False, ii, jj])

            num = sum(sth[0] for sth in curr_window)
            #print num, curr_window
            if num < 4:
                has_more_than_4_neighbours(curr_window, i, j)
                sparse.remove(pixel)
            else:
                if num < 6:
                    has_3_or_4_neighbours(curr_window, i, j)
                    sparse.remove(pixel)
                else:
                    if drenno and num == 6:
                        has_2_neighbours(curr_window, i, j)
                        sparse.remove(pixel)
        if l1 == len(sparse):
            drenno = True
        else:
            drenno = False


def colorize(original_img, marked_img, desired, form, m=1):  # m=1 - remove markers
    """
    Colorizes black-and-white image
    :return: returns nothing, saves colorized image as BMP file
    """

    global YUV, isColored, height, width, size, indices, w_radius, px_num_in_w
    global row_inds, col_inds, pixel_num, length, vals, colorized
    YUV, isColored = info_from_img(original_img, marked_img)

    height = YUV.shape[0]
    width = YUV.shape[1]
    size = height * width

    indices = np.arange(size).reshape(height, width, order="F").copy()

    w_radius = 1
    '''
    'O'
    '''
    px_num_in_w = (2 * w_radius + 1)**2
    max_num = size * px_num_in_w

    row_inds = np.zeros(max_num, dtype=np.int64)
    col_inds = np.zeros(max_num, dtype=np.int64)
    vals = np.zeros(max_num)

    length = pixel_num = 0  # p num - row index in sparse mtrx

    iterate()

    colorized = solve()

    if m:
        remove_markers()

    R, G, B = yiq_to_rgb(colorized[:,:,0], colorized[:,:,1], colorized[:,:,2])

    colorizedRGB = np.zeros(colorized.shape)
    colorizedRGB[:,:,0] = R
    colorizedRGB[:,:,1] = G
    colorizedRGB[:,:,2] = B

    #plt.imshow(colorizedRGB)
    #plt.show()
    misc.imsave(desired, colorizedRGB, format=form)


if __name__ == "__main__":
    colorize("original.bmp", "marked.bmp", "colorized.bmp", "bmp")