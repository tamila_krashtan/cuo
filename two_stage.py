"""
This program is created to colorize black-and-white images.
To use it you need to prepare marked image - the same image as grayscale btu with strokes of predefined colors
Required packages: SciPy, MatPlotLib, NumPy and PIL
"""

from scipy import misc
from scipy import sparse
from scipy.sparse import linalg
import matplotlib.pyplot as plt
import numpy as np
import colorsys
from PIL import Image


def yiq_to_rgb(y, i, q):    # colorsys has troubles with arrays
    """
    The function converts image from YIQ to RGB
    :param y: luma information
    :param i: orange-blue range
    :param q: purple-green range
    :return: set of arrays representing image in RGB model
    """
    r = y + 0.956 * i + 0.623 * q
    g = y - 0.272 * i - 0.648 * q
    b = y - 1.105 * i + 1.705 * q

    for ch in (r, g, b):
        ch[ch > 1] = 1
        ch[ch < 0] = 0

    return r, g, b


def info_from_img(o_img, m_img):
    """
    Obtains information about image by the names of original and marked variants
    :param o_img: path and name of original image
    :param m_img: path and name of marked image
    :return: combined YUV model representation as a float array and bool array showing pixels with defined colors
    """
    original = misc.imread(o_img)  # read image as numpy array
    marked = misc.imread(m_img)

    print original.shape, marked.shape

    original = original.astype(float) / 255
    marked = marked.astype(float) / 255

    isColored = abs(original - marked).sum(2) > 0.01  # if pixel is colored - numpy array

    y, _, _ = colorsys.rgb_to_yiq(original[:, :, 0], original[:, :, 1], original[:, :, 2])
    _, i, q = colorsys.rgb_to_yiq(marked[:, :, 0], marked[:, :, 1], marked[:, :, 2])

    yuv = np.zeros(original.shape)
    yuv[:, :, 0] = y
    yuv[:, :, 1] = i
    yuv[:, :, 2] = q

    return yuv, isColored


def statistics(w_vals, w_index):
    """
    Making statistics calculations needed on each iteration
    :param w_vals: values in current window
    :param w_index: index of last added value
    :return: returns nothing, modifies vals array
    """
    variance = np.mean((w_vals[0:w_index + 1] - np.mean(w_vals[0:w_index + 1])) ** 2)
    sigma = variance * 0.6

    mgv = min((w_vals[0:w_index + 1] - w_vals[w_index]) ** 2)  # +1? - diff with closest
    if sigma < (-mgv / np.log(0.01)):
        sigma = mgv / np.log(0.01)
    if sigma < 0.000002:  # not to divide by 0
        sigma = 0.000002

    # weighting func
    w_vals[0:w_index] = np.exp(-((w_vals[0:w_index] - w_vals[w_index]) ** 2) / sigma)
    # sum of weighting funcs should be 1
    w_vals[0:w_index] = w_vals[0:w_index] / np.sum(w_vals[0:w_index])
    vals[length - w_index:length] = -w_vals[0:w_index]


def iterate():
    """
    One iteration of method
    :return: returns nothing, modifies vals, row_inds, col_inds
    """
    global length, pixel_num

    for j in range(width):
        for i in range(height):
            if not isColored[i, j]:
                window_index = 0
                window_vals = np.zeros(px_num_in_w)

                for ii in range(max(0, i-w_radius), min(i+w_radius+1, height)):
                    for jj in range(max(0, j - w_radius), min(j + w_radius + 1, width)):
                        if ii != i or jj != j:
                            row_inds[length] = pixel_num
                            col_inds[length] = indices[ii, jj]
                            window_vals[window_index] = YUV[ii, jj, 0]  # copy luma at ii, jj
                            length += 1
                            window_index += 1

                center = YUV[i, j, 0]
                window_vals[window_index] = center

                statistics(window_vals, window_index)

            row_inds[length] = pixel_num
            col_inds[length] = indices[i, j]
            vals[length] = 1

            length += 1
            pixel_num += 1


def solve():
    """
    Building sparse matrix, solving obtained linear equation system and building final YUV representation
    :return: colorized image in YUV model
    """
    global vals, col_inds, row_inds

    vals = vals[0:length]
    col_inds = col_inds[0:length]
    row_inds = row_inds[0:length]

    A = sparse.csr_matrix((vals, (row_inds, col_inds)), (pixel_num, size))  # matrix from sparse
    b = np.zeros((A.shape[0]))

    colorized = np.zeros((YUV.shape))
    colorized[:, :, 0] = YUV[:, :, 0]

    color_copy = isColored.reshape(size, order="F").copy()
    colored_indices = np.nonzero(color_copy)

    for ch in [1, 2]:
        channel_color = YUV[:, :, ch].reshape(size, order="F").copy()
        b[colored_indices] = channel_color[colored_indices]

        col_vals = linalg.spsolve(A, b)
        colorized[:, :, ch] = col_vals.reshape(height, width, order="F")

    return colorized


def colorize1(original_img, marked_img, desired, form):
    """
    Colorizes black-and-white image
    :return: returns nothing, saves colorized image as BMP file
    """

    global YUV, isColored, height, width, size, indices, w_radius, px_num_in_w
    global row_inds, col_inds, pixel_num, length, vals, colorized
    YUV, isColored = info_from_img(original_img, marked_img)

    height = YUV.shape[0]
    width = YUV.shape[1]
    size = height * width

    indices = np.arange(size).reshape(height, width, order="F").copy()

    w_radius = 1
    '''
    'O'
    '''
    px_num_in_w = (2 * w_radius + 1)**2
    max_num = size * px_num_in_w

    row_inds = np.zeros(max_num, dtype=np.int64)
    col_inds = np.zeros(max_num, dtype=np.int64)
    vals = np.zeros(max_num)

    length = pixel_num = 0  # p num - row index in sparse mtrx

    iterate()

    colorized = solve()

    R, G, B = yiq_to_rgb(colorized[:,:,0], colorized[:,:,1], colorized[:,:,2])

    colorizedRGB = np.zeros(colorized.shape)
    colorizedRGB[:,:,0] = R
    colorizedRGB[:,:,1] = G
    colorizedRGB[:,:,2] = B

    #plt.imshow(colorizedRGB)
    #plt.show()
    misc.imsave(desired[:-4]+"_first.bmp", colorizedRGB, format=form)


def colorize(original_img, marked_img, desired, form):  # m=1 - remove markers
    global colorized, length, pixel_num

    colorize1(original_img, marked_img, desired, form)

    for j in range(width):
        for i in range(height):
            if isColored[i, j]:
                YUV[i, j, 1] = YUV[i, j, 1] = 0
            else:
                YUV[i, j, 1] = colorized[i, j, 1]
                YUV[i, j, 2] = colorized[i, j, 2]
            isColored[i,j] = not isColored[i,j]

    length = pixel_num = 0  # p num - row index in sparse mtrx

    iterate()

    colorized = solve()

    R, G, B = yiq_to_rgb(colorized[:, :, 0], colorized[:, :, 1], colorized[:, :, 2])

    colorizedRGB = np.zeros(colorized.shape)
    colorizedRGB[:, :, 0] = R
    colorizedRGB[:, :, 1] = G
    colorizedRGB[:, :, 2] = B

    # plt.imshow(colorizedRGB)
    # plt.show()
    misc.imsave(desired[:-4] + "_second.bmp", colorizedRGB, format=form)



if __name__ == "__main__":
    colorize("original.bmp", "marked.bmp", "colorized.bmp", "bmp")