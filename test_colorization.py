from cuo import info_from_img, check_image
from numpy.testing import assert_array_equal
import pytest

def test_info_from_img():
    yuv, isColored = info_from_img("for_tests/original.bmp", "for_tests/marked.bmp")
    expectedIC = [[True,True,True],
                  [True,True,True],
                  [True,True,False]]
    assert yuv[0,0,0] == 0
    assert yuv[1,2,0] == 0
    assert (yuv[2,0,0] - 1) < 0.0001
    assert_array_equal(isColored, expectedIC)


def test_pass_colored_image():
    with pytest.raises(Exception) as e_info:
        check_image("for_tests/marked.bmp")
    assert str(e_info.value) == 'Already colored image'

    with pytest.raises(Exception) as e_info:
        check_image("for_tests/corrupted.bmp")
    assert str(e_info.value) == 'Corrupted image'

    check_image("for_tests/original.bmp")


